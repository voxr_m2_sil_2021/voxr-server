![VoxR logo](.images/VoxR%20logo.png)

# VoxR API

An API to try and simplify the use of R.
This is also the server interface for the whole VoxR project.

## Installation

The simplest solution to launch and use the project is to use the docker file.
Here is the docker hub link:

🐳 [VoxR-server - docker hub](https://hub.docker.com/repository/docker/darthtomus/voxr-server)

You can also effectuate your tests by using the official API at this address (see Usage section):
`http://fluffserver.synology.me:5005/api/`

This is a solution for Visual Studio, written with [.Net 5.0](https://dotnet.microsoft.com).
You will need to have Microsoft Visual Studio installed:

[Visual Studio - Windows](https://visualstudio.microsoft.com/vs/)

[Visual Studio - Mac](https://visualstudio.microsoft.com/vs/mac/)

Alternatively, you can use [Jetbrains Rider](https://www.jetbrains.com/rider/), replacing the need for Visual Studio.

Just launch VoxR.API.sln with whatever IDE you chose, and you're good to go.

Nb: the simplest way to test the execution in an IDE is to use the dockerfile included within the program,
otherwise you will have to manually install R on your system, which can be a pain.

## Usage

| Method | Path                      | Input                                                               | Output         | Infos                                                                                                                            |
| ------ | ------------------------- | ------------------------------------------------------------------- | -------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| post   | /api/account/login        | string username, string password                                    | string         | Log the user in, returns a valid token                                                                                           |
| get    | /api/account/me           |                                                                     | user           | Returns the currently loged in user                                                                                              |
| post   | /api/account              | string username, string password, string firstname, string lastname | user           | Create the user if it does not already exist                                                                                     |
| delete | /api/R/deletRow           | string dataName, string\[\] rowSelect                               |                | Delete the selected rows, returns ok or the error of R                                                                           |
| delete | /api/R/deletCol           | string dataName, string\[\] columnSelect                            |                | Delete the selected columns, returns ok or the error of R                                                                        |
| delete | /api/R/deletData          | string dataName                                                     |                | Delete the selected data, returns ok or the error of R                                                                           |
| post   | /api/R/cropFrom           | string dataName, string\[\] columnSelect, string newDataName        |                | Initialize a new daataframe with the selected columns, return ok or the R error                                                  |
| post   | /api/R/insertRow          | string dataName, string\[\] newDataName                             |                | Insert row to dataFrame, returns ok or the error of R                                                                            |
| post   | /api/R/insertCol          | string dataName, string\[\] valueColum, string nameCol              |                | Insert column to dataFrame, returns ok or the error of R                                                                         |
| post   | /api/R/updateRowNum       | string dataName, string\[\] valueRow, int numRow                    |                | Update rows, returns ok or the error of R                                                                                        |
| post   | /api/R/updateColNum       | string dataName, string\[\] valuecCol, int numCol                   |                | Update columns from their number, returns ok or the error of R                                                                   |
| post   | /api/R/updateColName      | string dataName, string\[\] valuecCol, string nameCol               |                | Update columns from their names, returns ok or the error of R                                                                    |
| post   | api/R/filter              | string dataName, string condition, string newDataName               |                | Create a dataframe from a dataframe according to a condition, returns ok or the error of R                                       |
| post   | /api/R/saveData           | string dataName, string nameFile, string headerRow ( TRUE, FALSE)   |                | Creates a file with the name ask and asks the R service to save the dataframe selected from before, returns ok or the error of R |
| post   | api/R/command             | string command                                                      |                | Function to call the Rservice to interprets the user command                                                                     |
| post   | /api/R/setData            | string path, string dataName, string sep                            |                | Create a dataframe with the name chosen with a path or an url, returns ok or the error of R                                      |
| post   | api/R/replaceNaWithValue  | string dataName, string value                                       |                | Replace null values ​​with a constant, returns ok or the error of R                                                              |
| post   | api/R/asCaracterecColonne | string dataName, string nameCol                                     |                | Change column to type caractere, returns ok or the error of R                                                                    |
| post   | api/R/asNumericColonne    | string dataName, string nameCol                                     |                | Change column to type numeric, returns ok or the error of R                                                                      |
| post   | api/R/factorColonne       | string dataName, string nameCol                                     |                | Change column to type factor, returns ok or the error of R                                                                       |
| get    | /api/R/row                | string rowNumber, string dataName                                   | string\[\]     | Get row number X                                                                                                                 |
| get    | /api/R/column             | string colNumber, string dataName                                   | string\[\]     | Get column number X                                                                                                              |
| get    | /api/R/columnSom          | string dataName                                                     | string\[\]     | Get the sum of the columns                                                                                                       |
| get    | /api/R/columnName         | string dataName                                                     | string\[\]     | Get the names of the columns                                                                                                     |
| get    | /api/R/rowName            | string dataName                                                     | string\[\]     | Get the names of the rows                                                                                                        |
| get    | /api/R/statsData          | string dataName                                                     | string\[\]\[\] | Get the stats of the dataframe                                                                                                   |
| get    | /api/R/groupBy            | string dataName, string condition                                   | string\[\]\[\] | Get the result of a group\_by on the dataframe                                                                                   |
| get    | /api/R/select             | string dataName, string nameCol                                     | string\[\]\[\] | Get the result of a select on the dataframe                                                                                      |
| get    | /api/R/data               | string dataName                                                     | string\[\]\[\] | Get dataframe                                                                                                                    |
| get    | /api/R/MDS                | string dataName, string\[\] columnSelect, int dim                   | string\[\]\[\] | Get the projection of the selected numerical data                                                                                |
| get    | /api/R/getDataNames       |                                                                     | string\[\]     | Get the name of all initialized dataframe                                                                                        |
| get    | /api/R/getDataSize        | string dataName                                                     | string\[\]     | Get the size of the dataframe                                                                                                    |
| get    | /api/R/dataFromTo         | string dataName, int start, int end                                 |                | Initialize a new daataframe with the selected columns, return ok or the R error                                                  |

## Contributing

The VoxR team feels that its solution is not ready for contributions yet.
We can't wait to see what you have to offer though, and will open to contributions and explains how ASAP!

## Licence

[MIT](LICENCE)