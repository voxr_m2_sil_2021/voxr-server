using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VoxR.API.Configs;
using VoxR.Entity;
using VoxR.Service.Seeders;
using VoxR.Service.Services;

namespace VoxR.API
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContextPool<VoxRDbContext>(options =>
                options.UseSqlite(@"Data Source=./database.db")
            );

            services.AddHttpContextAccessor();
            services.AddScoped<IGenericControllerConfig, GenericControllerConfig>();
            services.AddScoped(_ => new TokenService("2LuKn3USiHILQabRjQk9m8ZU1axDoVAe", "VoxR"));
            services.AddScoped(typeof(GenericService<>));
            services.AddScoped<RService>();
            services.AddScoped<UserService>();
            services.AddScoped<UserSeeder>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseCors(builder =>
                builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
            );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.EnsureMigrationOfContext<VoxRDbContext>();
        }
    }
}