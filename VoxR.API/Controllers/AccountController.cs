﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VoxR.API.Configs;
using VoxR.Entity.Entities;
using VoxR.Entity.Models;
using VoxR.Service.Services;

namespace VoxR.API.Controllers
{
    // Used for anything login related
    [Route("api/[controller]")]
    public class AccountController : GenericController
    {
        private readonly UserService _userService;

        public AccountController(IGenericControllerConfig config, UserService userService) : base(config)
        {
            _userService = userService;
        }

        // Returns the currently connected user
        [HttpGet("me")]
        public ActionResult<User> Me()
        {
            if (!Authorized) return Unauthorized();
            var user = _userService.Get(x => x.Id == CurrentUserId);
            return Ok(user);
        }

        // Returns a valid token if the login model is correct, meaning the user exists and the password is correct
        [HttpPost("login")]
        [AllowAnonymous]
        public virtual ActionResult Login([FromBody] LoginModel login)
        {
            var res = _userService.GetToken(login);
            if (res == null) return Unauthorized();
            return Ok(new
            {
                token = res
            });
        }

        // Create a new user from the provided account model
        [HttpPost("")]
        [AllowAnonymous]
        public virtual ActionResult CreateAppAccount([FromBody] AccountModel payload)
        {
            var user = _userService.CreateIfNotExists(payload);
            if (user == null)
                return BadRequest(new
                {
                    error = "Cet email est déjà utilisée"
                });
            return Ok(user);
        }
    }
}