using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using VoxR.API.Configs;
using VoxR.Entity.Entities;
using VoxR.Service.Services;

namespace VoxR.API.Controllers
{
    // A generic controller, that needs to be extended, used to handle the connected user
    public class GenericController : Controller
    {
        protected bool Authorized;
        [CanBeNull] protected User CurrentUser;
        protected Guid CurrentUserId;
        protected IHttpContextAccessor Http;
        protected UserService UserService;

        public GenericController(IGenericControllerConfig config)
        {
            Authorized = false;
            if (config == null)
                throw new ArgumentNullException(nameof(config));
            Http = config?.HttpContextAccessor;

            UserService = config.UserService;
            if (Http == null)
                return;
            LoadHttpData();
        }

        // Decode the token in context (if it exists) to provide connected user informations
        private void LoadHttpData()
        {
            var token = Http?.HttpContext?.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token == null) return;
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("2LuKn3USiHILQabRjQk9m8ZU1axDoVAe");
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out var validatedToken);

                var jwtToken = (JwtSecurityToken) validatedToken;
                CurrentUserId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "jti").Value);
                CurrentUser = CurrentUserId != null && CurrentUserId != Guid.Empty ? UserService.Find(CurrentUserId) : null;
                Authorized = CurrentUserId != null && CurrentUserId != Guid.Empty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}