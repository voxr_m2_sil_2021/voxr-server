﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using VoxR.Service.Services;

namespace VoxR.API.Controllers
{
    // Used to receive customer requests
    [ApiController]
    [Route("api/[controller]")]
    public class RController : ControllerBase
    {
        private readonly RService _rService;

        public RController(RService rService)
        {
            _rService = rService;
        }

        //GET

        // Function calls the R service to get row number X
        [HttpGet("row")]
        public ActionResult<string[]> GetRow(string rowNumber, string dataName)
        {
            try
            {
                return Ok(_rService.GetRow(rowNumber, dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get column number X
        [HttpGet("column")]
        public ActionResult<string[]> GetColumn(string colNumber, string dataName)
        {
            try
            {
                return Ok(_rService.GetColumn(colNumber, dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the sum of the columns
        [HttpGet("columnSom")]
        public ActionResult<double[]> GetColumnSom(string dataName)
        {
            try
            {
                return Ok(_rService.GetColumnSom(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the names of the columns
        [HttpGet("columnName")]
        public ActionResult<string[]> GetColumnNames(string dataName)
        {
            try
            {
                return Ok(_rService.GetColumnNames(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the names of the rows
        [HttpGet("rowName")]
        public ActionResult<string[]> GetRowNames(string dataName)
        {
            try
            {
                return Ok(_rService.GetRowNames(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the stats of the dataframe
        [HttpGet("statsData")]
        public ActionResult<string[][]> GetStatsDataFrame(string dataName)
        {
            try
            {
                return Ok(_rService.GetStatsDataFrame(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the result of a group_by on the dataframe
        [HttpGet("groupBy")]
        public ActionResult<string[][]> GetGroupBy(string dataName, string condition)
        {
            try
            {
                var data = _rService.GetGroupBy(dataName, condition);
                return _rService.ChangeType(data);
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the result of a select on the dataframe
        [HttpGet("select")]
        public ActionResult<string[][]> GetSelect(string dataName, string nameCol)
        {
            try
            {
                var tmp = nameCol.Split("//");
                var data = _rService.GetSelect(dataName, tmp);
                return Ok(_rService.ChangeType(data));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get dataframe
        [HttpGet("data")]
        public ActionResult<string[][]> GetData(string dataName)
        {
            try
            {
                var data = _rService.GetDataFrame(dataName);
                return _rService.ChangeType(data);
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the projection of the selected numerical data
        [HttpGet("MDS")]
        public ActionResult<string[][]> GetProjectionMds(string dataName, string columnSelect, int dim)
        {
            try
            {
                var columnSelectTab = columnSelect.Split("//");
                var data = _rService.GetProjectionMds(dataName, columnSelectTab, dim);
                return Ok(_rService.ChangeType(data));
            }
            catch (Exception e)
            {
                var error = new string[1][];
                error[0] = new string[1];
                error[0][0] = e.Message;
                return error;
            }
        }

        // Test function to initialize a dataframe and return stats or R error
        [HttpGet("print")]
        public ActionResult<string[][]> Print()
        {
            try
            {
                var dataName = "aloa";
                var path = "https://github.com/poldham/opensource-patent-analytics/raw/master/2_datasets/ritonavir/ritonavir.csv";
                _rService.SetData(path, dataName, ",");
                return Ok(_rService.GetStatsDataFrame(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the name of all initialized dataframe
        [HttpGet("getDataNames")]
        public ActionResult<string[]> GetDataNames()
        {
            try
            {
                return Ok(_rService.GetDataNames());
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to get the size of the dataframe
        [HttpGet("getDataSize")]
        public ActionResult<string[]> GetDataSize(string dataName)
        {
            try
            {
                return Ok(_rService.GetDataSize(dataName));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to initialize a new dataframe with the selected columns, return ok or the R error
        [HttpGet("dataFromTo")]
        public ActionResult<string[][]> GetDataFromTo(string dataName, int start, int end)
        {
            try
            {
                var data = _rService.GetDataFromTo(dataName, start, end);
                return Ok(_rService.ChangeType(data));
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        //DELETE

        // Function calls the R service to delete the selected rows, returns ok or the error of R 
        [HttpDelete("deletRow")]
        public ActionResult DeleteRow(string dataName, string rowSelect)
        {
            try
            {
                // value send in the format a//b//c return  a string array
                var rowSelectTab = rowSelect.Split("//");
                _rService.DeleteRow(dataName, rowSelectTab);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to delete the selected columns, returns ok or the error of R
        [HttpDelete("deletCol")]
        public ActionResult DeleteCol(string dataName, string columnSelect)
        {
            try
            {
                var columnSelectTab = columnSelect.Split("//");
                _rService.DeleteCol(dataName, columnSelectTab);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to delete the selected data, returns ok or the error of R
        [HttpDelete("deletData")]
        public ActionResult DeleteData(string dataName)
        {
            try
            {
                _rService.DeleteData(dataName);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        //PUT

        // Function calls the R service to update rows, returns ok or the error of R
        [HttpPost("updateRowNum")]
        public ActionResult UpdateRowNum(string dataName, string valueRow, int numRow)
        {
            try
            {
                var valueRowTab = valueRow.Split("//");
                _rService.UpdateRowNum(dataName, valueRowTab, numRow);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to update columns from their number, returns ok or the error of R
        [HttpPost("updateColNum")]
        public ActionResult UpdateColNum(string dataName, string valuecCol, int numCol)
        {
            try
            {
                var valuecColTab = valuecCol.Split("//");
                _rService.UpdateColNum(dataName, valuecColTab, numCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to update columns from their names, returns ok or the error of R
        [HttpPost("updateColName")]
        public ActionResult UpdateColName(string dataName, string valuecCol, string nameCol)
        {
            try
            {
                var valuecColTab = valuecCol.Split("//");
                _rService.UpdateColName(dataName, valuecColTab, nameCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to insert row, returns ok or the error of R
        [HttpPost("insertRow")]
        public ActionResult InsertRow(string dataName, string valueRow)
        {
            try
            {
                var valueRowTab = valueRow.Split("//");
                _rService.InsertRow(dataName, valueRowTab);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to insert column, returns ok or the error of R
        [HttpPost("insertCol")]
        public ActionResult InsertCol(string dataName, string valueColum, string nameCol)
        {
            try
            {
                var valueColumTab = valueColum.Split("//");
                _rService.InsertCol(dataName, valueColumTab, nameCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to change column to type factor, returns ok or the error of R
        [HttpPost("factorColonne")]
        public ActionResult FactorColonne(string dataName, string nameCol)
        {
            try
            {
                _rService.FactorColonne(dataName, nameCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to change column to type numeric, returns ok or the error of R
        [HttpPost("asNumericColonne")]
        public ActionResult AsNumericColonne(string dataName, string nameCol)
        {
            try
            {
                _rService.AsNumericColonne(dataName, nameCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to change column to type caractere, returns ok or the error of R
        [HttpPost("asCaracterecColonne")]
        public ActionResult AsCaracterecColonne(string dataName, string nameCol)
        {
            try
            {
                _rService.FactorColonne(dataName, nameCol);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to replace null values with a constant, returns ok or the error of R
        [HttpPost("replaceNaWithValue")]
        public ActionResult ReplaceNaWithValue(string dataName, string value)
        {
            try
            {
                _rService.ReplaceNaWithValue(dataName, value);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }


        //POST

        // Function calls the R service to create a dataframe with the columns selected from another dataframe, returns ok or the error of R
        [HttpPost("cropFrom")]
        public ActionResult CropFrom(string dataName, string columnSelect, string newDataName)
        {
            try
            {
                var columnSelectTab = columnSelect.Split("//");
                _rService.CropFrom(dataName, columnSelectTab, newDataName);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to create a dataframe from a dataframe according to a condition, returns ok or the error of R
        [HttpPost("filter")]
        public ActionResult GetFilter(string dataName, string condition, string newDataName)
        {
            try
            {
                _rService.GetFilter(dataName, condition, newDataName);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function calls the R service to import the library, returns ok or the error of R
        [HttpPost("initR")]
        public ActionResult InitRservice()
        {
            try
            {
                _rService.InitRengine();
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function that calls the R service to create a dataframe with the name chosen with a path or an url, returns ok or the error of R
        [HttpPost("setData")]
        public ActionResult InitData(string path, string dataName, string sep)
        {
            try
            {
                _rService.SetData(path, dataName, sep);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function which creates a file with the name ask and asks the R service to save the dataframe selected from before, returns ok or the error of R
        [HttpPost("saveData")]
        public ActionResult SaveData(string dataName, string nameFile, string headerRow)
        {
            try
            {
                var pathString = Directory.GetCurrentDirectory();
                pathString = Path.Combine(pathString, nameFile + ".csv");
                System.IO.File.Create(pathString);
                Console.WriteLine(pathString);
                _rService.SaveFile(dataName, nameFile + ".csv", headerRow);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }

        // Function to call the Rservice to interprets the user command
        [HttpPost("command")]
        public ActionResult Command(string command)
        {
            try
            {
                _rService.Command(command);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new {error = e.Message});
            }
        }
    }
}