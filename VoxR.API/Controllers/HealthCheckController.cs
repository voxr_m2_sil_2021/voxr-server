using Microsoft.AspNetCore.Mvc;

namespace VoxR.API.Controllers
{
    // Used to check if the API is up
    [ApiController]
    [Route("api/[controller]")]
    public class HealthCheckController : ControllerBase
    {
        [HttpGet]
        public OkObjectResult Get()
        {
            return Ok("I'm alive");
        }
    }
}