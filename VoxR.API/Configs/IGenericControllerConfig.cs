using Microsoft.AspNetCore.Http;
using VoxR.Service.Services;

namespace VoxR.API.Configs
{
    // Interface for the config used by the GenericController during dependency injection
    public interface IGenericControllerConfig
    {
        IHttpContextAccessor HttpContextAccessor { get; set; }
        UserService UserService { get; set; }
    }
}