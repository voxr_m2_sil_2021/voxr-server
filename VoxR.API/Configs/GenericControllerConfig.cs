using Microsoft.AspNetCore.Http;
using VoxR.Service.Services;

namespace VoxR.API.Configs
{
    // Config used by the GenericController during dependency injection
    public class GenericControllerConfig : IGenericControllerConfig
    {
        public GenericControllerConfig(IHttpContextAccessor httpContextAccessor, UserService userService)
        {
            HttpContextAccessor = httpContextAccessor;
            UserService = userService;
        }

        public IHttpContextAccessor HttpContextAccessor { get; set; }
        public UserService UserService { get; set; }
    }
}