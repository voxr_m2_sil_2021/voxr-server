using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace VoxR.API
{
    public static class EnsureMigration
    {
        // This methode ensure the migrations are applied at the start of the program
        public static void EnsureMigrationOfContext<T>(this IApplicationBuilder app) where T : DbContext
        {
            using var scope = app.ApplicationServices.CreateScope();
            scope.ServiceProvider.GetService<T>()?.Database.Migrate();
        }
    }
}