using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VoxR.Service.Seeders;

namespace VoxR.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            host.StartAsync();

            var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            var userSeeder = services.GetRequiredService<UserSeeder>();
            userSeeder.Seed();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}