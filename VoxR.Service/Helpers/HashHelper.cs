using System;
using System.Security.Cryptography;
using System.Text;

namespace VoxR.Service.Helpers
{
    // Helper that provides some static methods to manipulate hash
    public static class HasherHelper
    {
        // Hash the provided string a SHA256 algorithm
        public static string Hash(string text)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrWhiteSpace(text))
                throw new ArgumentNullException(nameof(text));
            using (var shA256 = SHA256.Create())
            {
                var hash = shA256.ComputeHash(Encoding.UTF8.GetBytes(text));
                var stringBuilder = new StringBuilder();
                foreach (var num in hash)
                    stringBuilder.Append(num.ToString("x2"));
                return stringBuilder.ToString();
            }
        }
    }
}