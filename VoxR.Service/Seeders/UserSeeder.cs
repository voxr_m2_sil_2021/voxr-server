using System;
using VoxR.Entity.Entities;
using VoxR.Service.Helpers;
using VoxR.Service.Services;

namespace VoxR.Service.Seeders
{
    // Seeder used to add some user to the database at start, if it does not already exists
    public class UserSeeder
    {
        private readonly UserService _userService;

        public UserSeeder(UserService userService)
        {
            _userService = userService;
        }

        public void Seed()
        {
            var user1 = new User
            {
                Id = Guid.Parse("dd4e3260-1edd-43ea-af02-6546e02d0508"),
                FirstName = "Will",
                LastName = "Smith",
                Email = "will.smith@email.com",
                PasswordHash = HasherHelper.Hash("aze")
            };

            if (_userService.Get(x => x.Id == user1.Id) == null) _userService.Add(user1);
        }
    }
}