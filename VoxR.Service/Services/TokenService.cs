﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using VoxR.Entity.Entities;

namespace VoxR.Service.Services
{
    // Service to build tokens
    public class TokenService
    {
        private readonly string _issuer;
        private readonly string _key;

        public TokenService(string key, string issuer)
        {
            _key = key ?? throw new Exception("The 'Key' key in TokenService constructor is not valid");
            _issuer = issuer ?? throw new Exception("The 'issuer' key in TokenService constructor is not valid");
        }

        // Build a token, valid for 5 hours, for the specified user
        public string BuildToken(User user)
        {
            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Sub, user.Email),
                new(JwtRegisteredClaimNames.Jti, user.Id.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_issuer,
                _issuer,
                claims,
                expires: DateTime.Now.AddHours(5),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}