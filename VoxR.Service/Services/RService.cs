﻿using System;
using System.Collections.Generic;
using System.IO;
using RDotNet;

namespace VoxR.Service.Services
{
    // Used to call the R thread
    public class RService
    {
        private readonly Dictionary<string, DataFrame> _dataFrameDictionary = new();
        private REngine _engine;

        public RService()
        {
            InitRengine();
        }

        // Call the library() function of R to import the library
        public void InitRengine()
        {
            _engine = REngine.GetInstance();
            _engine.Initialize();
            _engine.Evaluate("library(readr)");
            _engine.Evaluate("library(berryFunctions)");
            _engine.Evaluate("library(dplyr)");
            _engine.Evaluate("library(tidyverse)");
        }

        // Call the R function read.csv and send it in a dataFrame with one chosen
        public void SetData(string path, string dataName, string sep)
        {
            _engine.Evaluate(dataName + " <- read.csv(\"" + path + "\",header=TRUE, sep =\"" + sep + "\")");
            _dataFrameDictionary.Add(dataName, _engine.Evaluate(dataName).AsDataFrame());
        }


        // Call the R function getColumn() and return it
        public string[] GetColumn(string colNumber, string dataName)
        {
            _engine.Evaluate("column <- getColumn(" + colNumber + ", " + dataName + ")");
            return _engine.Evaluate("column").AsCharacter().ToArray();
        }

        // Select the requested row and return it
        public string[] GetRow(string rowNumber, string dataName)
        {
            _engine.Evaluate("row <- " + dataName + "[" + rowNumber + ",]");
            return _engine.Evaluate("row").AsCharacter().ToArray();
        }

        // Call the R function colSums() and return it
        public double[] GetColumnSom(string dataName)
        {
            return _engine.Evaluate("colSums(" + dataName + ")").AsNumeric().ToArray();
        }

        // Retrieve the requested dataFrame
        public DataFrame GetDataFrame(string dataName)
        {
            return _engine.Evaluate(dataName).AsDataFrame();
        }

        // Retrieve the requested dataFrame between two terminals
        public DataFrame GetDataFromTo(string dataName, int start, int end)
        {
            return _engine.Evaluate(dataName + "[" + start + ":" + end + ",]").AsDataFrame();
        }

        // Call the R function colnames() and return it
        public string[] GetColumnNames(string dataName)
        {
            return _engine.Evaluate("colnames(" + dataName + ")").AsCharacter().ToArray();
        }

        // Call the R function rownames() and return it
        public string[] GetRowNames(string dataName)
        {
            return _engine.Evaluate("rownames(" + dataName + ")").AsCharacter().ToArray();
        }

        // Return the dataframes in memory if there is none, empty array return
        public string[] GetDataNames()
        {
            try
            {
                return _engine.Evaluate("names(which(unlist(eapply(.GlobalEnv,is.data.frame))))").AsCharacter().ToArray();
                ;
            }
            catch
            {
                return new string[0];
            }
        }

        // Call the R function dim() and return it
        public string[] GetDataSize(string dataName)
        {
            return _engine.Evaluate("dim(" + dataName + ")").AsCharacter().ToArray();
        }

        //Delet Fonction

        // Call the R function -c() for delete row
        public void DeleteRow(string dataName, string[] rowSelect)
        {
            var nameRow = "";
            for (var i = 0; i < rowSelect.Length; i++)
            {
                if (i > 0)
                    nameRow += ",";
                nameRow += rowSelect[i];
            }

            _engine.Evaluate(dataName + " <- " + dataName + "[-c(" + nameRow + "),]");
        }

        // Call the R function -c() for delete column
        public void DeleteCol(string dataName, string[] columnSelect)
        {
            var nameCol = "";
            for (var i = 0; i < columnSelect.Length; i++)
            {
                if (i > 0)
                    nameCol += ",";
                nameCol += columnSelect[i];
            }

            _engine.Evaluate(dataName + " <- " + dataName + "[,-c(" + nameCol + ")]");
        }

        // Call the R function rm() for delete dataframe
        public void DeleteData(string dataName)
        {
            _engine.Evaluate("rm(" + dataName + ")");
        }

        // Change the type of the selected column of a dataframe with the R function factor()
        public void FactorColonne(string dataName, string colonne)
        {
            _engine.Evaluate(dataName + "[," + colonne + "] <- factor(" + dataName + "[," + colonne + "])");
        }

        // Change the type of the selected column of a dataframe with the R function as.numeric()
        public void AsNumericColonne(string dataName, string colonne)
        {
            _engine.Evaluate(dataName + "[," + colonne + "] <- as.numeric(" + dataName + "[," + colonne + "])");
        }

        // Change the type of the selected column of a dataframe with the R function as.character()
        public void AsCaracterecColonne(string dataName, string colonne)
        {
            _engine.Evaluate(dataName + "[," + colonne + "] <- as.character(" + dataName + "[," + colonne + "])");
        }

        // Replace null values ​​with the chosen constant
        public void ReplaceNaWithValue(string dataName, string value)
        {
            _engine.Evaluate(dataName + "[is.na(" + dataName + ")] <- " + value);
        }

        // Add a row to the dataframe
        public void InsertRow(string dataName, string[] valueRow)
        {
            var valRow = "";
            for (var i = 0; i < valueRow.Length; i++)
            {
                if (i > 0)
                    valRow += ",";
                valRow += valueRow[i];
            }
            // Create a vector with c() and add it to the dataframe
            _engine.Evaluate("vec <- c(" + valRow + ")");
            _engine.Evaluate(dataName + "[nrow(" + dataName + ")+1,] <- vec");
        }

        // Add a column to the dataframe
        public void InsertCol(string dataName, string[] valueColum, string nameCol)
        {
            var valCol = "";
            for (var i = 0; i < valueColum.Length; i++)
            {
                if (i > 0)
                    valCol += ",";
                valCol += valueColum[i];
            }
            // Create a vector with c() and add it to the dataframe
            _engine.Evaluate("vec <- c(" + valCol + ")");
            _engine.Evaluate(dataName + "[\"" + nameCol + "\"] <- vec");
        }

        // Return in a new dataframe the result of select()
        public void CropFrom(string dataName, string[] columnSelect, string newDataName)
        {
            var nameStruct = "";
            for (var i = 0; i < columnSelect.Length; i++) nameStruct += "," + columnSelect[i];
            _engine.Evaluate(newDataName + " <- select(" + dataName + nameStruct + ")");
        }

        // Replace a row
        public void UpdateRowNum(string dataName, string[] valueRow, int numRow)
        {
            var valRow = "";
            for (var i = 0; i < valueRow.Length; i++)
            {
                if (i > 0)
                    valRow += ",";
                valRow += valueRow[i];
            }

            // Create a vector with c() and replace a row it to the dataframe
            _engine.Evaluate("vec <- c(" + valRow + ")");
            _engine.Evaluate(dataName + "[" + numRow + ",] <- vec");
        }

        // Replace a column with number
        public void UpdateColNum(string dataName, string[] valuecCol, int numCol)
        {
            var valCol = "";
            for (var i = 0; i < valuecCol.Length; i++)
            {
                if (i > 0)
                    valCol += ",";
                valCol += valuecCol[i];
            }

            _engine.Evaluate("vec <- c(" + valCol + ")");
            _engine.Evaluate(dataName + "[," + numCol + "] <- vec");
        }

        // Replace a column with name
        public void UpdateColName(string dataName, string[] valuecCol, string nameCol)
        {
            var valCol = "";
            for (var i = 0; i < valuecCol.Length; i++)
            {
                if (i > 0)
                    valCol += ",";
                valCol += valuecCol[i];
            }

            _engine.Evaluate("vec <- c(" + valCol + ")");
            _engine.Evaluate(dataName + "[, \"" + nameCol + "\" ] <- vec");
        }

        // Call the R function summary() and transform the result into a dataframe to retrieve it
        public string[][] GetStatsDataFrame(string dataName)
        {
            var stats = _engine.Evaluate("data.frame(summary(" + dataName + "))").AsDataFrame();
            var result = ChangeType(stats);
            var resultSansRowNumber = new string[result.Length][];
            // Remove the first box corresponding to the row number
            for (var i = 0; i < result.Length; i++)
            {
                var tmpRow = new string[result[i].Length - 1];
                for (var j = 1; j < result[i].Length; j++) tmpRow[j - 1] = result[i][j];
                resultSansRowNumber[i] = tmpRow;
            }

            return resultSansRowNumber;
        }

        // Call the R function write.csv() to save the dataframe in the chosen csv file
        public void SaveFile(string dataName, string pathFile, string headerRow)
        {
            _engine.Evaluate("write.csv(" + dataName + ",\"" + pathFile + "\", row.names = " + headerRow + ")");
        }

        // Call the R group_by() function with the condition chosen on the desired dataframe and return it
        public DataFrame GetGroupBy(string dataName, string condition)
        {
            var data = _engine.Evaluate(dataName + " %>% group_by(" + condition + ")").AsDataFrame();

            return data;
        }

        // Call the R select() function with the columns chosen on the desired dataframe and return it
        public DataFrame GetSelect(string dataName, string[] nameCol)
        {
            var nameStruct = "";
            for (var i = 0; i < nameCol.Length; i++) nameStruct += "," + nameCol[i];
            return _engine.Evaluate("select(" + dataName + nameStruct + ")").AsDataFrame();
        }

        // Call the R filter() function with the condition chosen on the desired dataframe and create new dataframe
        public void GetFilter(string dataName, string condition, string newDataName)
        {
            _engine.Evaluate(newDataName + "<-filter(" + dataName + "," + condition + ")");
        }

        // Creates a projection of desired dimension of the selected numeric columns of a dataframe
        public DataFrame GetProjectionMds(string dataName, string[] columnSelect, int dim)
        {
            var nameStruct = "";
            for (var i = 0; i < columnSelect.Length; i++) nameStruct += "," + columnSelect[i];

            // Create a dataframe with the selected columns
            _engine.Evaluate("tmpMXM" + dataName + " <- select(" + dataName + nameStruct + ")");
            // Creates a distance matrix of the created dataFrame
            _engine.Evaluate("tmpDXD" + dataName + " <- dist(tmpMXM" + dataName + ")");
            // Use the function cmdscale() on the distance matrix and put the result in a variable
            _engine.Evaluate("resultMDS" + dataName + " <- cmdscale(tmpDXD" + dataName + ",k=" + dim + ")");
            // Transform the result into a dataframe for the return
            return _engine.Evaluate("data.frame(resultMDS" + dataName + ")").AsDataFrame();
        }

        // Transform the dataFrame class into a two-dimensional string table
        public string[][] ChangeType(DataFrame data)
        {
            var colmnCount = data.ColumnCount;
            var dataInterface = new string[data.RowCount + 1][];

            dataInterface[0] = new string[colmnCount];

            for (var i = 0; i < colmnCount; i++) dataInterface[0][i] = data.ColumnNames[i];
            for (var numRow = 0; numRow < data.RowCount; numRow++)
            {
                dataInterface[numRow + 1] = new string[colmnCount];
                for (var numCol = 0; numCol < colmnCount; numCol++)
                    if (data[numRow, numCol] == null)
                        dataInterface[numRow + 1][numCol] = "null";
                    else
                        dataInterface[numRow + 1][numCol] = data[numRow, numCol].ToString();
            }

            return dataInterface;
        }

        // Transform the dataFrame class into a string table
        public string[] ChangeRow(DataFrame data)
        {
            var dataInterface = new string[data.ColumnCount];
            for (var numCol = 0; numCol < data.ColumnCount; numCol++) dataInterface[numCol] = data[0, numCol].ToString();
            return dataInterface;
        }

        // Function which interprets a user command
        public void Command(string command)
        {
            _engine.Evaluate(command);
        }

        //TODO Cross fonction
    }
}