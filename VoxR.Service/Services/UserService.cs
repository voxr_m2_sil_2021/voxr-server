﻿using System;
using VoxR.Entity;
using VoxR.Entity.Entities;
using VoxR.Entity.Models;
using VoxR.Service.Helpers;
using static System.String;

namespace VoxR.Service.Services
{
    // Service for the User class
    public class UserService : GenericService<User>
    {
        private readonly TokenService _tokenService;

        public UserService(VoxRDbContext dbContext, TokenService tokenService) : base(dbContext)
        {
            _tokenService = tokenService;
        }

        // Returns a token and authenticate the user
        public string GetToken(LoginModel model)
        {
            var user = Authenticate(model);
            if (user == null) return null;

            return _tokenService.BuildToken(user);
        }

        // Create and returns a user based on the model if none with the same username already exist
        public User CreateIfNotExists(AccountModel model)
        {
            if (Any(x => x.Email == model.Username)) return null;

            var user = new User
            {
                Id = Guid.NewGuid(),
                Email = model.Username,
                PasswordHash = HasherHelper.Hash(model.Password),
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            return Add(user);
        }

        // Return a user that match the provided login model if any exist
        private User Authenticate(LoginModel login)
        {
            if (IsNullOrEmpty(login.Password) || IsNullOrEmpty(login.Username)) return null;

            var encryptedPassword = HasherHelper.Hash(login.Password);
            return Get(x => x.Email == login.Username && x.PasswordHash == encryptedPassword);
        }
    }
}