using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VoxR.Entity;

namespace VoxR.Service.Services
{
    // A generic service, that needs to be extended, used to add database manipulation capabilities on T to other services
    public class GenericService<T> where T : class
    {
        private readonly VoxRDbContext _dbContext;

        public GenericService(VoxRDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // Returns a T that match the expression
        public T Get(Expression<Func<T, bool>> expression)
        {
            return _dbContext.Set<T>().Where(expression).FirstOrDefault();
        }

        // Returns all T that match the expression
        public IEnumerable<T> GetAll(Expression<Func<T, bool>> expression)
        {
            return _dbContext.Set<T>().Where(expression);
        }

        // Returns a T that match the id
        public T Find(Guid id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        // Add the object to the database
        public T Add(T obj)
        {
            _dbContext.Set<T>().Add(obj);
            _dbContext.SaveChanges();
            return obj;
        }

        // Returns true if any T in the database match the expression
        public bool Any(Expression<Func<T, bool>> expression)
        {
            return _dbContext.Set<T>().Any(expression);
        }
    }
}