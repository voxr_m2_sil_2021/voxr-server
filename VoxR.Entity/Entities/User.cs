using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace VoxR.Entity.Entities
{
    // User, used to authenticate
    public class User
    {
        [Required] public Guid Id { get; set; }

        [Required] public string FirstName { get; set; }

        [Required] public string LastName { get; set; }

        [Required] public string Email { get; set; }

        [JsonIgnore] public string PasswordHash { get; set; }
    }
}