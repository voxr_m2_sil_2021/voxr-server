namespace VoxR.Entity.Models
{
    public class AccountModel : LoginModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}