using Microsoft.EntityFrameworkCore;
using VoxR.Entity.Entities;

namespace VoxR.Entity
{
    // The DbContext for the app
    public class VoxRDbContext : DbContext
    {
        public VoxRDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}